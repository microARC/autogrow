#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup
# from _lib.pipenv.project import Project
# from _lib.pipenv.utils import convert_deps_to_pip
from pipenv.project import Project
from pipenv.utils import convert_deps_to_pip

pfile = Project().parsed_pipfile
requirements = convert_deps_to_pip(pfile['packages'], r=False)
test_requirements = convert_deps_to_pip(pfile['dev-packages'], r=False)

with open('README.md') as readme_file:
    readme = readme_file.read()

with open('HISTORY.md') as history_file:
    history = history_file.read()

setup(
    name='autogrow',
    version='0.1.0',
    description="Automatic plant growth control and health evaluation using computer vision",
    long_description=readme + '\n\n' + history,
    author="Michael Liao",
    author_email='s1070361@mail.ncyu.edu.tw',
    url='https://github.com/Michael-Liao/autogrow',
    packages=[
        'autogrow',
    ],
    package_dir={'autogrow':
                 'autogrow'},
    entry_points={
        'console_scripts': ['autogrow=autogrow.cli:main']
    },
    include_package_data=True,
    install_requires=requirements,
    license="MIT license",
    zip_safe=False,
    keywords='autogrow',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
    test_suite='tests',
    tests_require=test_requirements
)
