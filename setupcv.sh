#!/usr/bin/env bash
# inspired by Manu Ganji's post: https://bit.ly/2vfbnNv
# NOTE: run this script in a virtualenv
# Usage: ./setupcv.sh path/to/cv/source [OPTION]

# yes no option
yn=0
if [[ -z $VIRTUAL_ENV ]]; then
  echo "please activate your virtualenv"
  exit 1
fi
if [[ $# -lt 1 ]]; then
  # check arguments
  echo "please specify the path of opencv source"
  exit 2
elif [[ ! -f "$1/CMakeLists.txt" ]]; then
  echo "no CMakeLists to build"
  exit 3
elif [[ "$2" = "--yes" ]]; then
  echo "yes to all"
fi

# get default python version of virtualenv
py_version=$(python --version) # string: Python 3.5.2
major_version=${py_version:7:3} # substring: 3.5

mkdir -p $1/release-env
cd $1/release-env
# configure opencv to look for virtualenv
cmake -D MAKE_BUILD_TYPE=RELEASE \
      -D CMAKE_INSTALL_PREFIX=$VIRTUAL_ENV/ \
      -D PYTHON_EXECUTABLE=$VIRTUAL_ENV/bin/python \
      -D PYTHON_PACKAGES_PATH=$VIRTUAL_ENV/lib/python$major_version/site-packages \
      ..
# wait and check
if [[ !yn ]]; then
  read -p "is the configuration ok? [Y/n]:" yn
fi
case $yn in
  [yY]* ) break;;
  [nN]* ) exit;;
  * ) break;;
esac

make
make install
