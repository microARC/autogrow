# required by opencv
import numpy as np
from matplotlib import pyplot as plt
import cv2
# other stuff
import base64
import random # temp
#import json

#gray
# laptop camera: 0
# usb camera: 1

# cap = cv2.VideoCapture(cam)
# print('reading on {:d}'.format(cam))
# print('press \'q\' to abort')
#
# while cap.isOpened():
#     # capture frame-by-frame
#     ret, frame = cap.read()
#     if ret==True:
#         # grayscale
#         gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
#         cv2.imshow('grayscaled video',gray)
#
#         if cv2.waitKey(1) & 0xFF == ord('q'):
#             print('aborting')
#             break
#     else:
#         print('unable to read from capture')
#         break
#
# # when quit release capture
# cap.release()
# cv2.destroyAllWindows()

def process(cam):
    # read from cameras
    cap = cv2.VideoCapture(cam)
    ret, frame = cap.read()

    if ret == True:
    # TEMP: read from image for now
    #frame = cv2.imread('data/new_flush_dsz.jpg', cv2.IMREAD_COLOR)
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    # hue = np.zeros(hsv.size)
    # hue = hsv[:][:][0]
        hue = hsv[:,:,0]
    # gray = hsv[:,:,2].copy(order='C')
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    # hue = cv2.inRange(hue, 0, 255)
    #hue = cv2.equalizeHist(hue)

    #ret, bw = cv2.threshold(hue, 150,255, cv2.THRESH_BINARY)
    #ret, bw = cv2.threshold(hue, 40,80, cv2.THRESH_BINARY)
        bw = cv2.inRange(hue, 40, 80)
    # use square as structural element
        kernel = np.ones((3,3),np.uint8)
        bw_clean = cv2.morphologyEx(bw, cv2.MORPH_OPEN, kernel)

        count = np.count_nonzero(bw_clean==255)

    # test zone
    #plt.hist(hue.flatten(),256,[0,256])
    #plt.show()
    # print('area (px): {:d}'.format(count))
    # print('coverage (%): {:f}'.format(count/bw_clean.size))
    #cv2.imshow('clear', bw_clean)
    #cv2.imshow('hue', hue)
    #cv2.imshow('raw', frame)
    # NOTE: wait key is required to show, similar as the figure command in matlab
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()
        cap.release()
        return count/bw_clean.size, gray

    # default return
    cap.release()
    return 0, 0

def camEncode(imgMat):
    # _, img = cv2.imencode('.jpg', imgMat, [int(cv2.IMWRITE_JPEG_QUALITY), 80])
    _, img = cv2.imencode('.jpg', imgMat, [cv2.IMWRITE_JPEG_QUALITY, 80])
    byteString = base64.b64encode(img)
    imageString = byteString.decode('utf-8')

    return imageString
