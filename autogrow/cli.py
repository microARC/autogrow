# -*- coding: utf-8 -*-

import click
from autogrow.autogrow import start

@click.command()
@click.option('--host','-h', default='127.0.0.1')
def main(host):
    click.echo("connecting to broker at "+ host)
    start(host)

if __name__ == '__main__':
    main()
