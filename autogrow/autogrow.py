# -*- coding: utf-8 -*-
import numpy as np
import os # KeyboardInterrupt
import time # sleep
import random
import json
import base64

import paho.mqtt.client as mqtt

import autogrow.segment as segmt
import autogrow.comm as comm

SETTINGS = {}

# paho callbacks
def onConnect(client, userdata, flags, rc):
    print('connected with result code '+ str(rc))
    client.subscribe('autogrow/cmd')
    # client.publish('autogrow/sensor', '{:.5f}'.format(coverage))

def onMessage(client, userdata, msg):
    global SETTINGS
    print(msg.topic +": "+ str(msg.payload))
    SETTINGS = comm.retieveSettings(msg.payload)
    print(SETTINGS)

def onPublish(client, userdata, mid):
    print('published')
    # NOTE: do not sleep inside a callback, else nothing will be catched
    # time.sleep(3)

def onDisconnect(client, userdata, rc):
    client.loop_stop()
    print('disconnected with code: '+ str(rc))


def start(hostIP):
    # segmt.prepare()
    # print('show?')
    client = mqtt.Client()
    client.on_connect = onConnect
    client.on_message = onMessage
    client.on_publish = onPublish
    client.on_disconnect = onDisconnect

    client.connect(hostIP)

    # client.loop_forever()
    client.loop_start()
    resetCount = 0
    images = []
    imageStr = ''

    try:
        while True:
            # if publish==1:
            data = comm.collectSensors()
            # print('data collected')
            for i in range(3):
                coverage, gray = segmt.process(i)
                data['info'][i]['coverage'] = coverage
            #dataStr = json.dumps(data)
            # data.append(coverage)
            # dataStr = comm.formatData(data)
            #client.publish('autogrow/sensor', dataStr)
            # turn on the camera
                if bool(SETTINGS) and SETTINGS['camState']:
                    #if not gray.all(0):
                    images.append(segmt.camEncode(gray))
                    #print(images)
                    #imageStr = json.dumps(images)
                    #client.publish('autogrow/image', imageStr)
                    #else:
                    #    print('no image')
                elif bool(SETTINGS):
                    comm.response(SETTINGS)
                    resetCount += 1

            dataStr = json.dumps(data)
            client.publish('autogrow/sensor', dataStr)
            
            print(len(images))
            imageStr = json.dumps(images)
            if bool(images):
                print('published images')
                client.publish('autogrow/image', imageStr)
                images = []
                imageStr = ''

            if resetCount > 2:
                SETTINGS.clear()
                resetCount = 0
                print('all reset')
                comm.reset()
            time.sleep(3)

    except KeyboardInterrupt:
        print('\n!!escape!!')

    # gets keyboard interrupt signal and safely stop the loop :)
    client.loop_stop()
