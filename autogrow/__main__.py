# -*- coding: utf-8 -*-
""" allow execution via `python -m autogrow`."""

from autogrow.cli import main
# from .autogrow import main

if __name__ == "__main__":
    main()
