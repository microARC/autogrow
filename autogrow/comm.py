import datetime
import json
import random
import operator
import serial # reading from arduino

stream = serial.Serial('/dev/ttyUSB0', 9600)

def collectSensors():
    # get system time stamp
    # stream = serial.Serial('/dev/ttyACM0', 9600)
    ts = datetime.datetime.now().strftime('%H:%M:%S')
    # dummy random data
    temp = random.uniform(25, 34)
    moist = random.uniform(70, 95)
    lux = random.uniform(100, 200)
    # real reading data (untested)
    sensors = stream.readline()
    sensorStr = sensors.decode('utf-8')
    print(sensorStr[1:-2])

    # moisture1, moisture2, moisture3, temperature, humidity
    sensors = sensorStr[1:-2].split(',')
    if len(sensors) == 5:
        data = {
            'ts': ts,
            'airCondition': [sensors[3], sensors[4]],
            'info': [
                {'moisture': sensors[0]},
                {'moisture': sensors[1]},
                {'moisture': sensors[2]},
            ]
        }
        return data
    # return [ts, temp, moist, lux]

#def formatData(data):
#    dataDict = {
#        'ts': data[0],
#        'airCondition': data[1],
#        'moisture': data[2],
#        'luminance': data[3],
#        'coverage': data[4]
#    }
#
#    return json.dumps(dataDict)

def retieveSettings(byte):
    jsonStr = byte.decode('utf-8')
    obj = json.loads(jsonStr)
    # print(obj['light'])

    return obj

def response(setting):
    colorStr = ['w','w','w']
    waterState = ['0','0','0']
    for i, pot in enumerate(setting['pots']):
        color = pot['color']
        if color['r'] == color['g'] == color['b']:
            colorStr[i] = 'w'
        elif not pot['lights']:
            colorStr[i] = 'o'
        else:
            colorStr[i] = max(color.items(), key=operator.itemgetter(1))[0]
        
        # print(colorStr)

        if ':' in pot['water']:
            waterState[i] = '1'
        # print(waterState)

    toArduino = ''.join(colorStr).upper() + ''.join(waterState) +'\n'
    print(toArduino.encode('utf-8'))
    stream.write(toArduino.encode('utf-8'))
    #stream.write('RRR001\n'.encode())

def reset():
    stream.write('000\n'.encode())
