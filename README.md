# autogrow
a basic plant growth tracker using computer vision
in raspberry pi.

## Features
* automatic measurement of temperature & moist
* calculate plant size
* remote tracking over the Internet

## Usage
you should run it as a module
```
python -m autogrow
```

## Deployment
run the following command in the project directory
```
python setup.py bdist_wheel
```
**NOTE:** package info in `setup.py` is based on `Pipfile.lock`
you should make sure `pipenv` is installed before Deployment
it will generate a file with *whl* extension in the `dist` directory

## Installation
```
pip install autogrow-0.1.0-py2.py3-none-any.whl
```
**NOTE:** still needs `pipenv` (*for some wierd reason*)

## Requirements
* [OpenCV](https://github.com/opencv/opencv)
* [numpy](https://pypi.python.org/pypi/numpy/1.12.0)
* [matplotlib](https://pypi.org/project/matplotlib/)
* [pipenv](https://pypi.org/project/pipenv/)
